package com.qaproject.app.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class StartPage {

    private final SelenideElement topCategorySearchButton = $x("//button[@data-test-action='CategoriesClick']");

    public StartPage openStartPage(String url){
        open(url);
        return this;
    }

    public TopCategorySearch openTopCategorySearchPage(){
        topCategorySearchButton.click();
        return new TopCategorySearch();
    }

}
