package com.qaproject.app.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class ResultPage {

    private final SelenideElement resultCard = $x("//figure[@data-test-component='ProductCard']");

    private final ElementsCollection summaCollect = $$x("//figure[@data-test-component='ProductCard']//span[@data-test-component='Price']");
    private final ElementsCollection nameCollect = $$x("//figure[@data-test-component='ProductCard']//span[@data-test-block='ProductName']");

    private List<Integer> summa = new ArrayList<>();
    private List<String> names = new ArrayList<>();

    public ResultPage() throws InterruptedException {
        while(!resultCard.should().exists())
            Thread.sleep(1000);
    }

    public ResultPage getResult(){
        summaCollect.asFixedIterable().forEach(x->summa.add(Integer.valueOf(x.getText().replaceAll("[^\\d.]", ""))));
        nameCollect.asFixedIterable().forEach(x->names.add(x.getText()));
        return this;
    }

    public List<Integer> getSumma() {
        return summa;
    }

    public List<String> getNames() {
        return names;
    }
}
