package com.qaproject.app.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class TopCategorySearch {

    private final SelenideElement autoBrandOption = $x("//div[@data-test-id='auto_brand']");

    private final SelenideElement autoModelOption = $x("//div[@data-test-id='auto_model']");

    private final SelenideElement inputPriceFrom = $x("//input[@data-test-id='price_from']");

    private final SelenideElement inputPriceTo = $x("//input[@data-test-id='price_to']");

    public TopCategorySearch openCategorySelect(String str){
        $x("//div[@data-test-block='TopCategoriesList']//li[@data-test-id='"+str+"']").click();
        return this;
    }

    public TopCategorySearch openBrand(){
        autoBrandOption.click();
        return this;
    }

    public TopCategorySearch openBrandSelect(String str) throws InterruptedException {
        while(!$x("//*[local-name()='svg'][@data-test-id='"+str+"']").should().exists()){Thread.sleep(100);}
        $x("//*[local-name()='svg'][@data-test-id='"+str+"']").click();
        return this;
    }

    public TopCategorySearch openModel(){
        autoModelOption.click();
        return this;
    }

    public TopCategorySearch openModelSelect(String str) throws InterruptedException {
        while(!$x("//*[local-name()='svg'][@data-test-id='"+str+"']").should().exists()){Thread.sleep(100);}
        $x("//*[local-name()='svg'][@data-test-id='"+str+"']").click();
        return this;
    }

    public TopCategorySearch inputPriceFrom(String str){
        inputPriceFrom.sendKeys(str);
        return this;
    }

    public TopCategorySearch inputPriceTo(String str){
        inputPriceTo.sendKeys(str);
        return this;
    }

    public ResultPage resultOpen() throws InterruptedException {
        return new ResultPage();
    }
}
