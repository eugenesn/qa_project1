package com.qaproject.app;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.qaproject.app.pages.ResultPage;
import com.qaproject.app.pages.StartPage;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.*;

class AppApplicationTest {

    @BeforeEach
    public void setup(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @Test
    @Timeout(60)
    public void MainTest() throws InterruptedException {

        String url = "https://youla.ru/";
        String autoCategory = "100008";
        String toyotaBrand = "12040";
        String toyotaBrandName = "TOYOTA";
        String rav4Model = "12785";
        String rav4ModelName = "RAV4";
        Integer priceFrom = 1000000;
        Integer priceTo = 2000000;

        StartPage startPage = new StartPage();
        ResultPage resultPage = startPage.openStartPage(url)
                .openTopCategorySearchPage()
                .openCategorySelect(autoCategory)
                .openBrand()
                .openBrandSelect(toyotaBrand)
                .openModel()
                .openModelSelect(rav4Model)
                .inputPriceFrom(priceFrom.toString())
                .inputPriceTo(priceTo.toString())
                .resultOpen()
                .getResult();


        Assertions.assertTrue(Collections.max(resultPage.getSumma())<=priceTo);
        Assertions.assertTrue(Collections.min(resultPage.getSumma())>=priceFrom);

        resultPage.getNames().forEach(x->
                Assertions.assertTrue(x.toUpperCase().contains(toyotaBrandName)));
        resultPage.getNames().forEach(x->
                Assertions.assertTrue(x.toUpperCase().contains(rav4ModelName)));

    }
}